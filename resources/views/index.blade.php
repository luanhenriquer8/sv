<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <script id="hotmart_launcher_script">
        (function(l,a,u,n,c,h,e,r){l['HotmartLauncherObject']=c;l[c]=l[c]||function(){
            (l[c].q=l[c].q||[]).push(arguments)},l[c].l=1*new Date();h=a.createElement(u),
            e=a.getElementsByTagName(u)[0];h.async=1;h.src=n;e.parentNode.insertBefore(h,e)
        })(window,document,'script','//launcher.hotmart.com/launcher.js','hot');

        hot('account','1e715284-4fab-3c4a-9bac-0133fcd879e1');
    </script>

    <script id="mcjs">!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/ddde1542ca658cd8416492c2b/3c108a4426ebd9ba5931f743c.js");</script>

    <meta charset="utf-8">
    <title>StudiumVersatil</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="{{asset('/img/favicon.ico')}}">

    <!--Google Font link-->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet">


    <link rel="stylesheet" href="{{asset('/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('/css/iconfont.css/css/iconfont.css')}}">
    <link rel="stylesheet" href="{{asset('/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('/css/bootsnav.css')}}">


    <!--For Plugins external css-->
    <link rel="stylesheet" href="{{asset('/css/plugins.css')}}" />

    <!--Theme custom css -->
    <link rel="stylesheet" href="{{asset('/css/style.css')}}">
    <!--<link rel="stylesheet" href="assets/css/colors/maron.css">-->

    <!--Theme Responsive css-->
    <link rel="stylesheet" href="{{asset('/css/responsive.css')}}" />
    <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
    <script src="{{asset('/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js')}}"></script>
</head>
<body data-spy="scroll" data-target="#navbar-menu" data-offset="110">
<div id="loading">
    <div id="loading-center">
        <div id="loading-center-absolute">
            <div class="object" id="object_one"></div>
            <div class="object" id="object_two"></div>
            <div class="object" id="object_three"></div>
            <div class="object" id="object_four"></div>
        </div>
    </div>
</div><!--End off Preloader -->
<div class="culmn">
    <nav class="navbar navbar-default bootsnav navbar-fixed no-background white">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    {{--<i class="fa fa-search"></i>--}}
                </button>
                <a class="navbar-brand"  href="{{route('index')}}"><h1 style="font-family: 'Lobster', cursive; text-shadow: 2px 2px 8px #2e6da4;">David Gonsalves</h1>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#home">Sobre Mim</a></li>
                    <li><a href="#business">Youtube</a></li>
                    <li><a href="#product">Trabalhos</a></li>
                    <li><a href="#team">Habilidades</a></li>
                    <li><a href="#contact">Contato</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>
    </nav>
    <section  id="home" class="home bg-black fix">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="main_home">
                    <div class="col-md-12">
                        <div class="hello_slid">
                            <div class="slid_item xs-text-center">
                                <div class="col-sm-4">
                                </div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                            </div><!-- End off slid item -->
                        </div>
                    </div>
                </div>
            </div><!--End off row-->
        </div><!--End off container -->
    </section> <!--End off Home Sections-->
    <div class="container">
        <a href="{{ route('register') }}"  class="btn btn-primary m-top-20 center-block">Increva-se na Platarma e receba o seu ebook aqui</a>
        <p style="padding-top: 50px" class="text-center" style="font-size: 15px; padding-top: 10px">
            Meu nome é <b>David Gonsalves</b> desde 2005 atuo na área de beleza, comecei como vendedor, me tornei técnico e supervisor de uma indústria de cosméticos na qual atuei durante 5 anos.

            Saí desta empresa pra abrir meu próprio negocio (escola de cabeleireiros) além de ensinar a profissão eu também ajudo aos meus alunos a abrirem seus próprios negócios.

            Tive a oportunidade de conhecer alguns dos melhores cabeleireiros do brasil, durante 5 anos viajei por 22 capitais brasileiras ministrando cursos na área de química, colorimetria, cortes feminino, administração de salões Etc.

            Pude fazer cursos de especializações com...

            Mariano’s\ academia Mariano’s international

            Grazielle Gatto\ academia GATTOS

            Edson Borgo\ Empreendedor (o qual eu devo muito)fundador da HSB e Ecosmeticssalon

            Rino Farano, Marcelo Santana, Edson Freitas, Vanderley Nunes, Celso camasola, Cidinha Lins Etc.

            Já ministrei curso pela Embeleze, Senac, Fundação Dadalto, Fundação Bradesco, e já dei cursos pra muitos alunos que haviam ficado ano inteiro em determinadas escolas e não conseguia aprender, e eu consegui criar um método de ensino que o aluno aprende tudo o que precisa em apenas 3 meses e depois é só partir pra pratica, ensino desde como abrir uma empresa, contratar equipe, compra de produtos,  captar clientes e administrar seu negocio, de maneira simples(mas não fácil) se deseja ser um empreendedor tem que saber que é preciso colocar o time no campo e partir pro ataque.</p>
    </div>
    <section id="features" class="features bg-grey">
        <div class="container">
            <div class="row">
                <div class="main_features fix roomy-70">
                    <div style="padding-left: 60px; padding-right: 60px">
                        <div class="col-sm-8">
                            <div class="home_btns m-top-40">
                                <a style="visibility: hidden" href="{{url('/usuario/cadastrar')}}" class="btn btn-primary m-top-20">Inscreva-se na plataforma</a>
                                <a style="visibility: hidden" href="#team" class="btn btn-default m-top-20">Saber mais</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- End off row -->
        </div><!-- End off container -->
    </section><!-- End off Featured Section-->
    <section id="dialogue" class="dialogue bg-white roomy-80">
        <div class="container">
            <div class="row">
                <div class="main_dialogue text-center">
                    <div class="col-md-12">
                        <h3>Tenho trabalhado constantemente para oferecer um conteúdo de qualidade para você!</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="business" class="business bg-blue roomy-70">
        <div class="business_overlay"></div>
        <div class="container">
            <div class="row">
                <div class="main_business">
                    <div class="col-md-5">
                        <div class="business_item sm-m-top-50">
                            <h2 class="text-uppercase text-white"><strong>Nos</strong> acompanhe no Youtube</h2>
                            <div class="business_btn m-top-50">
                                {{--<a href="" class="btn btn-default m-top-20">Inscreve-se no canal do Youtube</a>--}}
                                <a href="https://www.youtube.com/channel/UC0wMbdfZssUN5F4EOJSyD8A"><img src="{{asset('/img/inscreva-se.png')}}"></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="business_item">
                            <div class="business_img">
                                <img src="{{asset('/img/youtube.png')}}" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End off Business section -->
    <section id="product" class="product text-center">
        <div class="container">
            <div class="main_product roomy-80">
                <div class="head_title text-center fix">
                    <h2 class="text-uppercase text-black">Workshops</h2>
                    <h5>Imagens de alguns de nossos eventos</h5>
                </div>
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="port_item xs-m-top-30">
                                            <div class="port_img">
                                                <img src="{{asset('/img/work1.jpg')}}" alt="" />
                                            </div>
                                            <div class="port_caption m-top-20">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="port_item xs-m-top-30">
                                            <div class="port_img">
                                                <img src="{{asset('/img/work2.jpg')}}" alt="" />
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="port_item xs-m-top-30">
                                            <div class="port_img">
                                                <img src="{{asset('/img/work3.jpg')}}" alt="" />
                                            </div>
                                            <div class="port_caption m-top-20">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="port_item xs-m-top-30">
                                            <div class="port_img">
                                                <img src="{{asset('/img/10.jpeg')}}" alt="" />
                                            </div>
                                            <div class="port_caption m-top-20">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="port_item xs-m-top-30">
                                            <div class="port_img">
                                                <img src="{{asset('/img/11.jpeg')}}" alt="" />
                                            </div>
                                            <div class="port_caption m-top-20">
                                                {{--<h5>Imagens capturadas no evento onde ganhei</h5>--}}
                                                {{--<h6>o grande prêmio <b style="font-size: 20px">"Tesoura de Ouro"</b>.</h6>--}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="port_item xs-m-top-30">
                                            <div class="port_img">
                                                <img src="{{asset('/img/12.jpeg')}}" alt="" />
                                            </div>
                                            <div class="port_caption m-top-20">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="head_title text-center fix"style="padding-top: 40px">
                        <h2 class="text-uppercase text-black">Antes e Depois</h2>
                    </div>
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="port_item xs-m-top-30">
                                            <div class="port_img">
                                                <img src="{{asset('/img/ad1.jpeg')}}" alt="" />
                                            </div>
                                            <div class="port_caption m-top-20">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="port_item xs-m-top-30">
                                            <div class="port_img">
                                                <img src="{{asset('/img/ad2.jpeg')}}" alt="" />
                                            </div>
                                            <div class="port_caption m-top-20">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="port_item xs-m-top-30">
                                            <div class="port_img">
                                                <img src="{{asset('/img/ad3.jpeg')}}" alt="" />
                                            </div>
                                            <div class="port_caption m-top-20">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="port_item xs-m-top-30">
                                            <div class="port_img">
                                                <img src="{{asset('/img/ad4.jpeg')}}" alt="" />
                                            </div>
                                            <div class="port_caption m-top-20">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="port_item xs-m-top-30">
                                            <div class="port_img">
                                                <img src="{{asset('/img/ad5.jpeg')}}" alt="" />
                                            </div>
                                            <div class="port_caption m-top-20">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="port_item xs-m-top-30">
                                            <div class="port_img">
                                                <img src="{{asset('/img/ad6.jpeg')}}" alt="" />
                                            </div>
                                            <div class="port_caption m-top-20">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="port_item xs-m-top-30">
                                            <div class="port_img">
                                                <img src="{{asset('/img/ad7.jpeg')}}" alt="" />
                                            </div>
                                            <div class="port_caption m-top-20">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="port_item xs-m-top-30">
                                            <div class="port_img">
                                                <img src="{{asset('/img/ad8.jpeg')}}" alt="" />
                                            </div>
                                            <div class="port_caption m-top-20">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="port_item xs-m-top-30">
                                            <div class="port_img">
                                                <img src="{{asset('/img/ad9.jpeg')}}" alt="" />
                                            </div>
                                            <div class="port_caption m-top-20">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="port_item xs-m-top-30">
                                            <div class="port_img">
                                                <img src="{{asset('/img/ad10.jpe    g')}}" alt="" />
                                            </div>
                                            <div class="port_caption m-top-20">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- End off row -->
        </div><!-- End off container -->
    </section><!-- End off Product section -->
    <section id="product" class="product text-center">
        <div class="container">
            <div class="main_product roomy-80">
                <div class="head_title text-center fix">
                    <h2 class="text-uppercase text-black">Nosso Próximo Evento</h2>
                </div>
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="container">
                                <div class="row">
                                    <iframe width="450" height="260" src="https://www.youtube.com/embed/uNPhaDh7Pvo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- End off row -->
        </div><!-- End off container -->
    </section><!-- End off Product section -->
    <section id="team" class="team bg-grey fix">
        <div class="container">
            <div class="main_team roomy-80">
                <div class="head_title text-center fix">
                    <h2 class="text-uppercase">Espertises</h2>
                    <h5></h5>
                </div>
                <div id="team-slid" class="carousel slide carousel-fade" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="team_item team_skill">
                                            <div class="team_author">
                                                <h4>David Gonsalves</h4>
                                                <p>Cabeleireiro</p>
                                            </div>
                                            <div class="team_skill_title fix m-top-40 m-bottom-40">
                                                <h5>Habilidades</h5>
                                            </div>
                                            <div class="skill_bar sm-m-top-50 m-top-20">
                                                <div class="teamskillbar clearfix m-top-20" data-percent="80%">
                                                    <h6 class="text-uppercase">Corte Masculino</h6>
                                                    <div class="teamskillbar-bar"></div>
                                                </div> <!-- End Skill Bar -->
                                                <div class="teamskillbar clearfix m-top-50" data-percent="75%">
                                                    <h6 class="text-uppercase">Corte Feminino</h6>
                                                    <div class="teamskillbar-bar"></div>
                                                </div> <!-- End Skill Bar -->
                                                <div class="teamskillbar clearfix m-top-50" data-percent="90%">
                                                    <h6 class="text-uppercase">Mega Hair</h6>
                                                    <div class="teamskillbar-bar"></div>
                                                </div> <!-- End Skill Bar -->
                                                <div class="teamskillbar clearfix m-top-50" data-percent="75%">
                                                    <h6 class="text-uppercase">Empreendedor</h6>
                                                    <div class="teamskillbar-bar"></div>
                                                </div> <!-- End Skill Bar -->
                                            </div><!-- End off skill bar -->

                                        </div>
                                    </div><!-- End off col-sm-4 -->
                                    <div class="col-sm-4">
                                        <div class="team_item xs-m-top-30">
                                            <div class="port_img">
                                                <img src="{{asset('/img/1.jpg')}}" alt="" />
                                            </div>
                                        </div>
                                    </div><!-- End off col-sm-4 -->
                                    <div class="col-sm-4">
                                        <div class="team_item team_content xs-m-top-30">
                                            <div class="team_socail">
                                            </div>
                                        </div>
                                    </div><!-- End off col-sm-4 -->
                                </div><!-- End off row -->
                            </div>
                        </div><!-- End off team -->
                    </div>
                </div>
            </div><!-- End off row -->
        </div><!-- End off container -->
    </section><!-- End off Product section -->

    <section id="test" class="test bg-black roomy-60 fix">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="main_test fix text-center">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="head_title text-center fix">
                            <h2 class="text-uppercase text-white">Prêmio Recebido</h2>
                            <h5 class="text-white"></h5>
                        </div>
                    </div>
                    <div id="testslid" class="carousel slide carousel-fade" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="test_item fix">
                                        <div class="test_img fix">
                                            <img class="img-circle" src="{{asset('/img/premio1.jpg')}}" alt="" />
                                        </div>

                                        <div class="test_text text-white">
                                            <em>Vencedor do prêmio tesoura de ouro com ênfase nacional.</em>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- End off item -->
                        </div><!-- End off carosel inner -->
                    </div>
                </div>
            </div><!-- End off row -->
        </div><!-- End off container -->
    </section><!-- End off test section -->
    <!-- Begin MailChimp Signup Form -->
    <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
    <style type="text/css">
        #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
        /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
           We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
    </style>
    <div id="mc_embed_signup">
        <form action="https://studiumversatil.us18.list-manage.com/subscribe/post?u=ddde1542ca658cd8416492c2b&amp;id=1124d25489" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <div id="mc_embed_signup_scroll">
                <h2>Quer receber nossas novidades? Inscreva-se!</h2>
                <div class="indicates-required"><span class="asterisk"></span> </div>
                <div class="mc-field-group">
                    <label for="mce-EMAIL">E-mail<span class="asterisk"></span>
                    </label>
                    <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                </div>
                <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_ddde1542ca658cd8416492c2b_1124d25489" tabindex="-1" value=""></div>
                <div class="clear"><input type="submit" value="Inscreva-se" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
            </div>
        </form>
    </div>
    <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';fnames[5]='BIRTHDAY';ftypes[5]='birthday';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
    <!--End mc_embed_signup-->
    <footer id="contact" class="footer action-lage bg-black p-top-80">
        <!--<div class="action-lage"></div>-->
        <div class="container">
            <div class="row">
                <div class="widget_area">
                    <div class="col-md-3">
                        <div class="widget_item widget_about">
                            <h5 class="text-white">Nosso Endereço</h5>
                            <div class="widget_ab_item m-top-30">
                                <div class="item_icon"><i class="fa fa-location-arrow"></i></div>
                                <div class="widget_ab_item_text">
                                    <h6 class="text-white">Location</h6>
                                    <p>
                                        Avenida Do Bocage, Número 08, Santo André Setúbal, <br>CEP 2830-003</p>
                                </div>
                            </div>
                            <div class="widget_ab_item m-top-30">
                                <div class="item_icon"><i class="fa fa-phone"></i></div>
                                <div class="widget_ab_item_text">
                                    <h6 class="text-white">Contato :</h6>
                                    <p>+351 939 950 773</p>
                                </div>
                            </div>
                            <div class="widget_ab_item m-top-30">
                                <div class="item_icon"><i class="fa fa-envelope-o"></i></div>
                                <div class="widget_ab_item_text">
                                    <h6 class="text-white">Email :</h6>
                                    <p>studiumversatil@gmail.com</p>
                                </div>
                            </div>
                        </div><!-- End off widget item -->
                    </div><!-- End off col-md-3 -->
                    <div class="col-md-3">
                    </div><!-- End off col-md-3 -->
                    <div class="col-md-3">
                    </div><!-- End off col-md-3 -->
                    <div class="col-md-3">
                    </div><!-- End off col-md-3 -->
                </div>
            </div>
        </div>
        <div class="main_footer fix bg-mega text-center p-top-40 p-bottom-30 m-top-80">
            <div class="col-md-12">
                <p class="wow fadeInRight" data-wow-duration="1s">
                    Made with
                    <i class="fa fa-heart"></i>
                    by
                    <a target="_blank" href="http://bootstrapthemes.co">Bootstrap Themes</a>
                    2016. All Rights Reserved
                </p>
            </div>
        </div>
    </footer>
</div>
<script src="{{asset('/js/vendor/jquery-1.11.2.min.js')}}"></script>
<script src="{{asset('/js/vendor/bootstrap.min.js')}}"></script>
<script src="{{asset('/js/jquery.magnific-popup.js')}}"></script>
<script src="{{asset('/js/jquery.easing.1.3.js')}}"></script>
<script src="{{asset('/js/bootsnav.js')}}"></script>
<script src="{{asset('/js/plugins.js')}}"></script>
<script src="{{asset('/js/main.js')}}"></script>


</body>
</html>
