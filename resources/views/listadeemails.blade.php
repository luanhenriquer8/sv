@extends('layouts.blocosuperior')
<?php $i = 1 ?>
@section('content')
    <section class="container" style="padding-top: 100px; padding-bottom: 100px">
        <h1 class="text-center">Lista de Clientes Cadastrados em</h1>
        <h1 class="text-center">sua base de dados</h1>
        <div style="padding-top: 50px">
            <table class="table table-bordered" style="padding-top: 50px">
                <thead>
                <tr style="background-color: #2e6da4">

                    <th style="color: white">Num</th>
                    <th style="color: white">Nome do cliente</th>
                    <th style="color: white">E-mail</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{strtoupper($user->name)}}</td>
                        <td>{{strtoupper($user->email)}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <label>Quantidade total de Clientes: <b
                        style="padding-left: 10px; font-size: 25px">{{count($users)}}</b></label>
        </div>
    </section>

@stop



