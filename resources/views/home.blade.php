@extends('layouts.blocosuperior')

@section('content')

    <form style="padding-top: 50px" action="{{ url('/email-enviado') }}" method="post">
        {{csrf_field()}}
        <h2 style="text-align: center">Olá <b>David</b>, esta é a sua plataforma!</h2>
        <div class="center-block" style="padding-top: 100px; padding-bottom: 100px">
            <ul>
                <li><a href="{{ route('listEmails') }}" class="btn btn-primary btn-lg btn-block">Clique aqui para ver sua lista de clientes cadastrados</a></li>
                <li><a href="{{ route('enviarEmail') }} " class="btn btn-secondary btn-lg btn-block">Clique aqui enviar um e-mail à um cliente</a></li>
                <li><a class="btn btn-primary btn-lg btn-block">Clique aqui para voltar à página inicial</a></li>
            </ul>
        </div>
    </form>

@endsection
