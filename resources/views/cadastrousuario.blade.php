@extends('layouts.blocosuperior')

@section('content')

    <form action="{{route('salvaUsuario')}}" method="post">
        {{csrf_field()}}
        <div class="container">
            <h1 class="text-center">Você está a um passo de receber o nosso conteúdo</h1>
        </div>

        <div class="container" style="padding-top: 30px">
            <h2 class="text-center text-info">Dados do usuário</h2>
            <div class="form-group">
                <label for="nome">Nome Completo: </label>
                <input id="nome" name="nome" placeholder="Aqui entra seu nome completo" class="form-control" maxlength="30">
            </div>
            <div class="row">
                <div class="col">
                    <label for="telefoneDoUsuario">Telefone: </label>
                    <input type="email"  id="email" placeholder="Aqui entra seu email" name="email" class="form-control">
                </div>
            </div>
            <div style="padding-top: 10px">
                <button class="btn btn-primary">Salvar</button>
            </div>
        </div>
    </form>


@stop



