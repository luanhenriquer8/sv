@extends('layouts.blocosuperior')

@section('content')

    @if (Session::has('message'))
        <h3 style="padding-top: 10px" class="alert alert-info text-center">{{ Session::get('message') }}</h3>
    @endif>


    <form style="padding-top: 50px" action="{{ url('/email-enviado') }}" method="post" style="padding-top: 70px; padding-bottom: 70px">
    {{csrf_field()}}
    <h2 style="text-align: center">Enviar para email para cliente</h2>
    <div style="padding-left: 200px; padding-right: 200px; padding-top: 100px; padding-bottom: 70px">
        <div class="form-group">
            <label>Email do Destinatário</label>
            <input name="dest" type="text" class="form-control" placeholder="Escreva o nome do destinatário">
        </div>
        <div class="form-group">
            <label >Assunto</label>
            <input name="assunto" type="text" class="form-control" placeholder="Escreva o assunto da sua mensagem">
        </div>
        <div class="form-group">
            <label>Mensagem</label>
            <textarea name="mensagem" class="form-control"  placeholder="Aqui você escreve a mensagem para o seu cliente"></textarea>
        </div>
        <div style="display: flex; justify-content: center; padding-top: 20px">
            <button type="submit" class="btn btn-primary">Enviar Mensagem</button>

        </div>
    </div>
</form>

@stop



