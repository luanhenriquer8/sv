<?php
/**
 * Created by PhpStorm.
 * User: luanhenriquer8
 * Date: 26/02/18
 * Time: 20:23
 */

namespace App\Http\Controllers;


class MailController extends Controller
{
    public function send(Request $request){
        Mail::send(['text' => $request->input('mensagem')], ['name', $request->input('nome')], function ($message) use($request){
            $message->to('studiumversatil@gmail.com', 'Teste')->subject('Teste site studiumversatil');
            $message->from($request->input('email'), 'Studium Versatil');
        });
    }

    public function sendEmailWithEbook(Request $request){
        Mail::send(['text' => $request->input('mensagem')], ['name', $request->input('nome')], function ($message) use($request){
            $message->to('studiumversatil@gmail.com', 'Teste')->subject('Teste site studiumversatil');
            $message->from($request->input('email'), 'Studium Versatil');
        });
    }

}