<?php
/**
 * Created by PhpStorm.
 * User: luanhenriquer8
 * Date: 26/02/18
 * Time: 21:53
 */

namespace App\Http\Controllers;


use App\Usuario;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{
    public function salvaUsuario(Request $request){
        $usuario = new  Usuario();
        $usuario->nome = $request->input('nome');
        $usuario->email = $request->input('email');
        $usuario->save();

        return view('index');
    }

    public function redirecioCadastroUsuario(){
        return view('cadastrousuario');
    }
}