<?php
/**
 * Created by PhpStorm.
 * User: luanhenriquer8
 * Date: 26/02/18
 * Time: 20:22
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use App\Email;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class EmailController extends Controller
{
    public function enviarEmail(Request $request){

        $email = new Email();

        $email->mensagem                    = $request->input('mensagem');
        $email->nome                        = 'David Gonsalves';
        $email->assunto                     = $request->input('assunto');
        $email->destinatario                = $request->input('dest');

        Mail::send('email', ['x' => $email->mensagem], function ($m) use ($email){
            $m->from('studiumversatil@gmail.com', $email->nome);
            $m->to($email->destinatario, $email->destinatario)->subject($email->assunto);

        });

        $message = "Email Enviado com Sucesso";

        return redirect()->route('enviarEmail')->with(Session::flash('message', 'Email enviado com sucesso!'));
    }
}