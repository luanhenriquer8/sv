<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\RegisterController;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index');
    }

    public function redirecionarRegister(){
        return view('auth/register');
    }

    public function redirecionarMenuAdministrador(){
        return view('menuadministrador');
    }

    public function formEnviarEmailCliente(){
        return view('enviaremailcliente');
    }

    public function redirecionaIndex(){
        return view('index');
    }

    public function redirecioCadastroUsuario(){
        return view('cadastrousuario');
    }


    public function redirecionarListaEmails(){
        $users = User::all();
        return view('listadeemails', compact('users'));
    }
}
