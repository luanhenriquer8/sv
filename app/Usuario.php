<?php
/**
 * Created by PhpStorm.
 * User: luanhenriquer8
 * Date: 26/02/18
 * Time: 21:52
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    public $timestamps = false;
}