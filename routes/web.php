<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/cadastrar', 'HomeController@redirecionarRegister')->name('cadastrar');
Auth::routes();

Route::get('/', function () {
    return view('index');
});

Route::get('/home', function () {
    return view('index');
})->name('index');

Route::get('/welcome-as-admin', function () {
    return view('welcome');
});

Route::get('/login-to-the-admin-page', function () {
    return view('auth.login');
});


Route::group(['prefix' => 'api'], function()
{
    Route::group(['middleware' => 'auth'], function()
    {
        Route::post('/salvar-usuario', 'UsuarioController@salvarUsuario')->name('salvaUsuario');
    });
});

Route::get('/cadastro-usuario', 'UsuarioController@redirecioCadastroUsuario')->name('cadastrousuario');


Route::get('/administrador/enviar-email-para-cliente', function () {
    return view('enviaremailcliente');
});

Route::post('/menu-administrador', 'HomeController@redirecionarMenuAdministrador')->name('menuAdmin');

Route::get('/enviar-email-para-cliente', 'HomeController@formEnviarEmailCliente')->name('enviarEmail');

Route::post('/email-enviado', 'EmailController@enviarEmail');

Route::get('/enviar-email-para-cliente', 'HomeController@formEnviarEmailCliente')->name('enviarEmail');

Route::get('/lista-de-emails', 'HomeController@redirecionarListaEmails')->name('listEmails');

Route::get('/area-as-admin', 'HomeController@redirecionarMenuAdministrador')->name('areaDoAdministrador');


